# Ejercicio Scheduler Order

Refer to the exhibits. The Batch Job processes, filters, and aggregates records. What is the outcome from the Logger component when the flow is executed?

![alt text](image.png)

- [ ] [10,20] [30,40] [50,60]
- [ ] [10,20,30,40,50,60]
- [x] [20,40] [60]
- [ ] [20,40,60]

**Respuesta correcta:** [20,40] [60]